<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\RfidTag */

$this->title = 'Create Rfid Tag';
$this->params['breadcrumbs'][] = ['label' => 'Rfid Tags', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rfid-tag-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
