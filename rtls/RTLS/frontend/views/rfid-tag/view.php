<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\RfidTag */

$this->title = $model->rfid_tag_id;
$this->params['breadcrumbs'][] = ['label' => 'Rfid Tags', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rfid-tag-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->rfid_tag_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->rfid_tag_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'rfid_tag_id',
            'rfid_tag_no',
            'antenna',
            'latitude',
            'longitude',
        ],
    ]) ?>

</div>
