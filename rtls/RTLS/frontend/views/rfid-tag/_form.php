<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\RfidTag */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="rfid-tag-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'rfid_tag_no')->textInput() ?>

    <?= $form->field($model, 'antenna')->textInput(['maxlength' => 50]) ?>

    <?= $form->field($model, 'latitude')->textInput(['maxlength' => 50]) ?>

    <?= $form->field($model, 'longitude')->textInput(['maxlength' => 50]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
