<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\RfidTag */

$this->title = 'Update Rfid Tag: ' . ' ' . $model->rfid_tag_id;
$this->params['breadcrumbs'][] = ['label' => 'Rfid Tags', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->rfid_tag_id, 'url' => ['view', 'id' => $model->rfid_tag_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="rfid-tag-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
