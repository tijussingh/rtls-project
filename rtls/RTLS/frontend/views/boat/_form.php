<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\Boat */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="boat-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'jetty')->textInput(['maxlength' => 50]) ?>

    <?= $form->field($model, 'reg_date')->textInput(['maxlength' => 20]) ?>

    <?= $form->field($model, 'ren_date')->textInput(['maxlength' => 20]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Next' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
