<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\Boat */

$this->title = 'Update Boat: ' . ' ' . $model->reg_no;
$this->params['breadcrumbs'][] = ['label' => 'Boats', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->reg_no, 'url' => ['view', 'id' => $model->reg_no]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="boat-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
