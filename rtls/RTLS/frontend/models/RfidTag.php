<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "rfid_tag".
 *
 * @property integer $rfid_tag_id
 * @property integer $rfid_tag_no
 * @property string $antenna
 * @property string $latitude
 * @property string $longitude
 */
class RfidTag extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'rfid_tag';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['rfid_tag_no', 'antenna', 'latitude', 'longitude'], 'required'],
            [['rfid_tag_no'], 'integer'],
            [['antenna', 'latitude', 'longitude'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'rfid_tag_id' => 'Rfid Tag ID',
            'rfid_tag_no' => 'Rfid Tag No',
            'antenna' => 'Antenna',
            'latitude' => 'Latitude',
            'longitude' => 'Longitude',
        ];
    }
}
