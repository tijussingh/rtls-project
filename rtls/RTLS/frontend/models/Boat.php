<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "boat".
 *
 * @property integer $reg_no
 * @property string $jetty
 * @property string $reg_date
 * @property string $ren_date
 */
class Boat extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'boat';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['jetty', 'reg_date', 'ren_date'], 'required'],
            [['jetty'], 'string', 'max' => 50],
            [['reg_date', 'ren_date'], 'string', 'max' => 20]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'reg_no' => 'Reg No',
            'jetty' => 'Jetty',
            'reg_date' => 'Registration Date',
            'ren_date' => 'Renewal Date',
        ];
    }
}
