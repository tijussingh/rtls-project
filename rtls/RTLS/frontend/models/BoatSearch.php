<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\Boat;

/**
 * BoatSearch represents the model behind the search form about `frontend\models\Boat`.
 */
class BoatSearch extends Boat
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['reg_no'], 'integer'],
            [['jetty', 'reg_date', 'ren_date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Boat::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'reg_no' => $this->reg_no,
        ]);

        $query->andFilterWhere(['like', 'jetty', $this->jetty])
            ->andFilterWhere(['like', 'reg_date', $this->reg_date])
            ->andFilterWhere(['like', 'ren_date', $this->ren_date]);

        return $dataProvider;
    }
}
