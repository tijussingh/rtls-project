<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\RfidTag;

/**
 * RfidTagSearch represents the model behind the search form about `frontend\models\RfidTag`.
 */
class RfidTagSearch extends RfidTag
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['rfid_tag_id', 'rfid_tag_no'], 'integer'],
            [['antenna', 'latitude', 'longitude'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = RfidTag::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'rfid_tag_id' => $this->rfid_tag_id,
            'rfid_tag_no' => $this->rfid_tag_no,
        ]);

        $query->andFilterWhere(['like', 'antenna', $this->antenna])
            ->andFilterWhere(['like', 'latitude', $this->latitude])
            ->andFilterWhere(['like', 'longitude', $this->longitude]);

        return $dataProvider;
    }
}
