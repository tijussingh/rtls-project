<?php

namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;


class formsyncController extends Controller
{
    public function actionIndex()
    {
       $boat = \frontend\models\Boat::findOne(Yii::$app->user->getId());
       $owner = \frontend\models\Owner::findOne(Yii::$app->user->getId());
       $rfid_tag=  \frontend\models\RfidTag::findOne(Yii::$app->user->getId());
        if(is_null($boat))
        {
            $this->redirect(Yii::$app->request->baseUrl.'/index.php?r=boat/create',302);
        }
        else if(is_null($owner))
        {
            $this->redirect(Yii::$app->request->baseUrl.'/index.php?r=owner/create',302);
        }
         else if(is_null($rfid_tag))
        {
            $this->redirect(Yii::$app->request->baseUrl.'/index.php?r=rfid-tag/create',302);
        }
       
        return $this->render('index');
        
    }

}
