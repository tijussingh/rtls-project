<?php

namespace frontend\controllers;

use Yii;
use yii\web\Controller;


class myinitController extends Controller
{
    public function actionIndex()
    {
        if(Yii::$app->user->isGuest)
        {
            return $this->redirect(Yii::$app->request->baseUrl.'/index.php?r=site/index',302);
        }
        else
        {
            return $this->redirect(Yii::$app->request->baseUrl.'/index.php?r=boat/create',302);
        }
        return $this->render('index');
    }

}
