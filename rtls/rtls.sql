-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 17, 2015 at 07:38 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `rtls`
--

-- --------------------------------------------------------

--
-- Table structure for table `boat`
--

CREATE TABLE IF NOT EXISTS `boat` (
`reg_no` int(11) NOT NULL,
  `jetty` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `reg_date` varchar(20) COLLATE latin1_general_ci NOT NULL,
  `ren_date` varchar(20) COLLATE latin1_general_ci NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `boat`
--

INSERT INTO `boat` (`reg_no`, `jetty`, `reg_date`, `ren_date`) VALUES
(1, 'fsadf', 'sdfs', 'dfs'),
(2, 'Bandra', '12/14/2014', '12/15/1201');

-- --------------------------------------------------------

--
-- Table structure for table `migration`
--

CREATE TABLE IF NOT EXISTS `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1424151764),
('m130524_201442_init', 1424151766);

-- --------------------------------------------------------

--
-- Table structure for table `owner`
--

CREATE TABLE IF NOT EXISTS `owner` (
`id` int(11) NOT NULL,
  `name` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `address` varchar(70) COLLATE latin1_general_ci NOT NULL,
  `contact_no` int(11) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `owner`
--

INSERT INTO `owner` (`id`, `name`, `address`, `contact_no`) VALUES
(1, 'sujit', 'mumbai', 123456789),
(2, 'sujit', 'mumbai', 123456789),
(3, 'sujit', 'mumbai', 123456789);

-- --------------------------------------------------------

--
-- Table structure for table `rfid_tag`
--

CREATE TABLE IF NOT EXISTS `rfid_tag` (
`rfid_tag_id` int(11) NOT NULL,
  `rfid_tag_no` varchar(20) COLLATE latin1_general_ci NOT NULL,
  `antenna` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `latitude` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `longitude` varchar(50) COLLATE latin1_general_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
`id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Sujit', 'aIquH3-kQpryL-l7EHt0-fFRZOsCVFFb', '$2y$13$WlKPR44qbVFiH7F1CGlCu.dONUl9t/08CkSzNTnYWpP8XuGv5udIW', NULL, 'tijussingh@gmail.com', 10, 1424154640, 1424154640),
(2, 'tijus', '4uMho8rK4oo9KBSePRvtFPOo_CPsAYmG', '$2y$13$TlHHxACi73JfQTKAy/w8cOTMWUxHiylWC1B3KGMeRm5HWQw3pIfjy', NULL, 'sujit.singh@somaiya.edu', 10, 1424154741, 1424154741);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `boat`
--
ALTER TABLE `boat`
 ADD PRIMARY KEY (`reg_no`);

--
-- Indexes for table `migration`
--
ALTER TABLE `migration`
 ADD PRIMARY KEY (`version`);

--
-- Indexes for table `owner`
--
ALTER TABLE `owner`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rfid_tag`
--
ALTER TABLE `rfid_tag`
 ADD PRIMARY KEY (`rfid_tag_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `boat`
--
ALTER TABLE `boat`
MODIFY `reg_no` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `owner`
--
ALTER TABLE `owner`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `rfid_tag`
--
ALTER TABLE `rfid_tag`
MODIFY `rfid_tag_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
